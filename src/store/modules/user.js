import { login, getInfo, getUserAvatar } from '@/api/user'
import { getToken, setToken, removeToken } from '@/utils/auth'
// import { resetRouter } from '@/router'

const getDefaultState = () => {
  return {
    // getToken用于获取本来就已经存在的token
    token: getToken(),
    name: '',
    avatar: '',
    userInfo: null
  }
}

const state = getDefaultState()

const mutations = {
  RESET_STATE: (state) => {
    Object.assign(state, getDefaultState())
  },
  SET_TOKEN: (state, token) => {
    state.token = token
  },
  SET_NAME: (state, name) => {
    state.name = name
  },
  SET_AVATAR: (state, avatar) => {
    state.avatar = avatar
  },
  // 更新用户信息
  updateUserInfo(state, info) {
    state.userInfo = info
  }
}

const actions = {
  // 实现一个action用来登录
  async login ({ commit }, form) {
    // context类似于$store
    // payload是触发action时传递的参数
    try {
      const ret = await login({
        mobile: form.username,
        password: form.password
      })
      if (ret.code === 10000) {
        // 登录成功,先保存到state里面，再缓存token到本地存储
        commit('SET_TOKEN', ret.data)
        setToken(ret.data)
        return Promise.resolve(true)
      } else {
        // 登录失败
        return Promise.reject(false)
      }
    } catch {
      return Promise.reject(false)
    }
  },
  // 获取用户信息，头像
  async getInfo({ commit }) {
    // 调接口获取userinfo的信息
    const baseInfo = await getInfo()
    // console.log('baseInfo', baseInfo)
    // 调头像接口，拿到头像数据
    const detailInfo = await getUserAvatar(baseInfo.data.userId)
    // console.log('detailInfo', detailInfo)

    if (baseInfo.code === 10000) {
      // 获取用户信息成功
      commit('updateUserInfo', {
        ...baseInfo.data,
        ...detailInfo.data
      })
    }
  },
  logout({ commit }) {
    // 清空token
    commit('SET_TOKEN', '')
    // 清空用户信息
    commit('updateUserInfo', '')
    // 删除缓存的token
    removeToken()
  }
  // get user info
  // 获取用户的信息
  // getInfo ({ commit, state }) {
  //   return new Promise((resolve, reject) => {
  //     getInfo(state.token).then(response => {
  //       const { data } = response
  //       if (!data) {
  //         return reject('Verification failed, please Login again.')
  //       }

  //       const { name, avatar } = data

  //       commit('SET_NAME', name)
  //       commit('SET_AVATAR', avatar)
  //       resolve(data)
  //     }).catch(error => {
  //       reject(error)
  //     })
  //   })
  // },

  // user logout
  // logout ({ commit, state }) {
  //   return new Promise((resolve, reject) => {
  //     logout(state.token).then(() => {
  //       removeToken() // must remove  token  first
  //       resetRouter()
  //       commit('RESET_STATE')
  //       resolve()
  //     }).catch(error => {
  //       reject(error)
  //     })
  //   })
  // },

  // remove token
  // resetToken ({ commit }) {
  //   return new Promise(resolve => {
  //     removeToken() // must remove  token  first
  //     commit('RESET_STATE')
  //     resolve()
  //   })
  // }
}

export default {
  // 命名空间，把模块变成局部的
  namespaced: true,
  state,
  mutations,
  actions
}
