const getters = {
  sidebar: state => state.app.sidebar,
  device: state => state.app.device,
  token: state => state.user.token,
  // staffPhoto
  avatar: state => state.user.userInfo && state.user.userInfo.staffPhoto,
  name: state => state.user.name,
  username: state => state.user.userInfo && state.user.userInfo.username
}
export default getters
