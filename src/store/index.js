import Vue from 'vue'
import Vuex from 'vuex'
// import getters from './getters'
// 默认导入三个模块
import app from './modules/app'
import settings from './modules/settings'
import user from './modules/user'
// 导入全局模块
import global from './modules/global'

Vue.use(Vuex)

const store = new Vuex.Store({
  // 这里的是全局模块
  ...global,
  // 局部模块
  modules: {
    app,
    settings,
    user
  }
})

export default store
