const instance = () => {
  return 123
}

// 导出一个默认的函数
export default () => {
  // console.log('hello')
  return instance({
    method: 'get',
    url: '',
    data: ''
  })
}

// ---------------------------------
// ES6规定：对象的key可以是变量
const method = 'post'
const key = method === 'get' ? 'params' : 'data'
const obj = {
  [key]: 'nihao',
  msg: 'hello'
}
// 对象属性的访问方式有两种
// 1. 对象.属性
// 2. 对象[属性名称]
// 上述两者的唯一区别是：方括号方式支持变量
// console.log(obj.msg)
// console.log(obj['msg'])
// const key = 'msg'
// console.log(obj[key])

// const abc = 'hi'
// obj[abc] = 111
console.log(obj)
