// 封装通用的接口调用模块
import axios from 'axios'
import store from '@/store'
import { removeToken } from '@/utils/auth'
import router from '@/router'

// 基准地址
// const baseURL = '/dev-api'
// const baseURL = 'http://ihrm-java.itheima.net/api'
const baseURL = process.env.VUE_APP_BASE_API

// 创建一个独立的实例
const instance = axios.create({
  baseURL: baseURL
})

instance.interceptors.request.use(config => {
// Do something before request is sent
// 添加请求头
  const token = store.state.user.token
  if (token) {
    config.headers.Authorization = `Bearer ${token}`
  }
  return config
}, error => {
// Do something with request error
  return Promise.reject(error)
})

// 添加响应拦截器
instance.interceptors.response.use(res => {
  // res.data表示后端返回的原始数据
  return res.data
}, (err) => {
  // 判断是否为失效的token
  if (err.response.status === 401 && err.response.data.code === 10002) {
    // 清token
    store.commit('user/SET_TOKEN', '')
    // 清空用户信息
    store.commit('user/updateUserInfo', null)
    // 删除缓存的token
    removeToken()
    // 跳转登录页
    router.push('/login')
  }
  return Promise.reject(err)
})

// export default instance

// 封装通用的接口调用函数
// 按照这种方式封装时，无论何种请求方式，参数都用data传递，更加方便
export default (options) => {
  let key = null
  if (options.method) {
    // method属性是存在的
    key = options.method.toUpperCase() === 'GET' ? 'params' : 'data'
  } else {
    // 如果没有传递method属性，默认采用get请求方式
    key = 'params'
  }
  return instance({
    // 短路运算
    method: options.method || 'get',
    url: options.url,
    // data用于传递除get之外的所有请求方式的数据：post/put/delete
    // data: options.data,
    // params仅仅处理get请求参数
    // params: options.params
    // ES6规则：对象的key可以是动态的变量
    [key]: options.data
  })
}
