// 自定义Vue插件
import PageTools from '@/components/PageTools/index.vue'
// 导入组件
import UploadExcel from '@/components/UploadExcel'
import moment from 'moment'
export default {
  install (Vue) {
    // 配置全局组件
    Vue.component('page-tools', PageTools)
    // 导入注册excel组件，进行组件的全局注册
    Vue.component(UploadExcel.name, UploadExcel)

    // 扩展过滤器
    Vue.filter('formatDate', (value) => {
      return moment(value).format('yyyy-MM-DD')
    })
  }

}
