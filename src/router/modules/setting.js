// 部门管理路由模块
import Layout from '@/layout'

export default {
  path: '/setting',
  component: Layout,
  children: [{
    // 如果二级路由的path映射地址是空字符串，那么默认显示该二级路由组件
    path: '',
    name: 'setting',
    component: () => import('@/views/setting/index'),
    meta: { title: '角色管理', icon: 'setting' }
  }]
}
