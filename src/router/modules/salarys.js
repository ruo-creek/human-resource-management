// 部门管理路由模块
import Layout from '@/layout'

export default {
  path: '/salarys',
  component: Layout,
  children: [{
    // 如果二级路由的path映射地址是空字符串，那么默认显示该二级路由组件
    path: '',
    name: 'salarys',
    component: () => import('@/views/salarys/index'),
    meta: { title: '工资', icon: 'money' }
  }]
}
