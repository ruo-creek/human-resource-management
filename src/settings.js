module.exports = {
  // 控制页面的标题（修改标题需要重启）
  title: 'happy cdd',

  /**
   * @type {boolean} true | false
   * @description Whether fix the header
   */
  // 控制顶部导航是否固定
  fixedHeader: true,

  /**
   * @type {boolean} true | false
   * @description Whether show the logo in sidebar
   */
  // 控制左侧菜单栏顶部是否出现Logo
  sidebarLogo: true
}
