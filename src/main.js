import Vue from 'vue'
// 类似于reset.css文件，用于屏蔽浏览器之间样式上的差异
import 'normalize.css/normalize.css' // A modern alternative to CSS resets
// 导入ElementUI相关的配置
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
// 配置默认的语言：默认英文
import locale from 'element-ui/lib/locale/lang/zh-CN' // lang i18n
// 导入样式文件
import '@/styles/index.scss' // global css
// 导入根组件
import App from './App'
// 导入Vuex
import store from './store'
// 导入路由
import router from './router'
// 导入图标
import '@/icons' // icon
// 导入权限控制逻辑
import '@/permission' // permission control
// 导入自定义组件PageTools然后配置该插件
import MyPlugin from '@/utils/plugins.js'
Vue.use(MyPlugin)

// ES6模块化导入和导出的规则
// import request from '@/utils/m1.js'
// const ret = request()
// console.log(ret)

/**
 * If you don't want to use mock-server
 * you want to use MockJs for mock api
 * you can execute: mockXHR()
 *
 * Currently MockJs will be used in the production environment,
 * please remove it before going online ! ! !
 */
// 模拟后端接口（忽略）
if (process.env.NODE_ENV === 'production') {
  const { mockXHR } = require('../mock')
  mockXHR()
}

// 配置ElementUI的插件并且设置中文环境
Vue.use(ElementUI, { locale })
// 如果想要中文版 element-ui，按如下方式声明
// Vue.use(ElementUI)

Vue.config.productionTip = false

new Vue({
  el: '#app',
  router,
  store,
  // 把根组件App渲染到index.html页面中的id是app的div里面
  render: (h) => h(App)
})
