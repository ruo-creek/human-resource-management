// 导入default属性,然后起一个别名直接导出
// 组件的本质其实是对象
// 导入这三个组件时，可以简化为一行
export { default as Navbar } from './Navbar'
export { default as Sidebar } from './Sidebar'
export { default as AppMain } from './AppMain'

// 导入一个属性，属性名称是default
// (1) import { default } from ' ./Navbar
// (2) export const Navbar = default
// 等价于==>   export { default as Navbar } from' . /Navbar'
