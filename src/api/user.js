import request from '@/utils/request'
// 1. 无论何种请求方式，都用data传递参数
// 2. method的值不区分大小写
// 3. method可以省略，默认采用get请求

export function login (data) {
  return request({
    url: '/sys/login',
    method: 'post',
    data
  })
}

// 获取用户的信息
export function getInfo (token) {
  return request({
    url: '/sys/profile',
    method: 'post'
  })
}

export function logout () {
  return request({
    url: '/vue-admin-template/user/logout',
    method: 'post'
  })
}

// 获取用户的头像
export function getUserAvatar (id) {
  return request({
    method: 'get',
    url: '/sys/user/' + id
  })
}
