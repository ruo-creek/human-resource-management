import request from '@/utils/request'

// 获取员工的简单列表
export function getEmployeeSimple() {
  return request({
    url: '/sys/user/simple'
  })
}

// 获取员工列表数据
// options 一个是page: 1,一个size: 10
export function reqGetEmployeeList(options) {
  return request({
    methods: 'get',
    url: '/sys/user',
    data: options
  })
}

// 删除员工
export function reqDelEmployee(id) {
  return request({
    method: 'delete',
    url: `/sys/user/${id}`
  })
}

// 新增员工
export function reqAddEmployee(data) {
  return request({
    method: 'post',
    url: '/sys/user',
    data
  })
}
