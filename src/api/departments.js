import request from '@/utils/request'
// 接口：获取原始的组织架构列表
export function reqGetDepartments() {
  return request({
    url: '/company/department'
  })
}

// 接口：删除组织架构数据

// 删除部门
// 新增部门-基本功能

export function reqDelDepartments(id) {
  return request({
    method: 'DELETE',
    url: '/company/department/' + id
  })
}

// 新增部门,提交表单
export function reqAddDepartments(data) {
  return request({
    url: '/company/department',
    method: 'post',
    data
  })
}

// 获取部门详情数据
export function reqGetDepartDetail(id) {
  return request({
    url: `/company/department/${id}`
  })
}

// 编辑部门
export function reqUpdateDepartments(data) {
  return request({
    url: `/company/department/${data.id}`,
    method: 'put',
    data
  })
}
