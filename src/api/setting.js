import request from '@/utils/request'
/* 获取角色列表**/
export function reqGetRoleList(options) {
  return request({
    method: 'get',
    url: '/sys/role',
    data: options
  })
}

// 封装删除角色的api
export function reqDeleteRole(id) {
  return request({
    url: `/sys/role/${id}`,
    method: 'delete'
  })
}

// 封装新增角色功能api
export function reqAddRole(data) {
  return request({
    url: '/sys/role',
    data,
    method: 'post'
  })
}

// 修改角色
export function reqUpdateRole(data) {
  return request({
    method: 'put',
    url: `/sys/role/${data.id}`,
    data
  })
}

// 获取角色详情
export function reqGetRoleDetail(id) {
  return request({
    method: 'get',
    url: `/sys/role/${id}`
  })
}
